package br.com.w2s.api.controlecaixa.service;

/**
 * @author Rodrigo Mazoni
 * @date 20 de mar. de 2023
 * @version 1.0.0
 * @description <p> Interface Service Base para ser herdada por todas interfaces e classes da camada services </p>
 *
 *
 */
public interface IServiceBase {

}
