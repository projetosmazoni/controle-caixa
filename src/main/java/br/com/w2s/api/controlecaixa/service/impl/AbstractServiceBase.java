package br.com.w2s.api.controlecaixa.service.impl;

import br.com.w2s.api.controlecaixa.domain.model.ModelMapperBase;
import br.com.w2s.api.controlecaixa.service.IServiceBase;

/**
 * @author Rodrigo Mazoni
 * @date 20 de mar. de 2023
 * @version 1.0.0
 * @description <p> Classe Service Base de implementacao </p>
 *
 *
 */
public abstract class AbstractServiceBase extends ModelMapperBase implements IServiceBase {

	private static final long serialVersionUID = 8985847323896234162L;

}
