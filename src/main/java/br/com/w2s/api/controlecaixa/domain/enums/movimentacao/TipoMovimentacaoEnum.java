package br.com.w2s.api.controlecaixa.domain.enums.movimentacao;

import br.com.w2s.api.controlecaixa.domain.entity.caixa.CaixaContaEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Rodrigo Mazoni
 * @date 20 de mar. de 2023
 * @version 1.0.0
 * @description <p> Enum utilizado para guardar os possiveis tipos de movimentacao de {@link CaixaContaEntity} </p>
 *
 *
 */
@Getter
@AllArgsConstructor
public enum TipoMovimentacaoEnum {
	
	CREDITO(0, "Crédito"), DEBITO(1, "Débito");
	
	private int codigo;
	private String descricao;

}
