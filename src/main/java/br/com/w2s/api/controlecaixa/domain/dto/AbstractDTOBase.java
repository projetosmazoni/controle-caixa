package br.com.w2s.api.controlecaixa.domain.dto;

/**
 * @author Rodrigo Mazoni
 * @date 20 de mar. de 2023
 * @version 1.0.0
 * @description <p> Classe DTO Base para ser herdada por todas classes ModelMapper de entidades JPA </p>
 *
 *
 * @param <ID>
 */
public abstract class AbstractDTOBase {

}
