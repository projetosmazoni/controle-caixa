package br.com.w2s.api.controlecaixa.domain.dto.filter;

import org.springframework.data.domain.Pageable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Rodrigo Mazoni
 * @date 20 de mar. de 2023
 * @version 1.0.0
 * @description <p> Classe Filter SQL Base DTO para ser extendidas pelas classes filters SQL do sistema </p>
 *
 *
 */
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class FilterSQLBaseDTO {

	private String query;
	private Pageable pageable;
	
}
