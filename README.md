Controle de Caixa API

Introdução: O Presente documento tem como objetivo descrever o projeto Controle de Caixa API e suas especificações. O sistema tem como finalidade criar cadastros de caixas e contas bancárias e realizar lançamentos de CRÉDITO e DÉBITO em um caixa ou conta bancária específico. O sistema permite criar quantos caixas e contas bancárias forem necessários. Também é permitido realizar uma conciliação bancária, confirmando assim o valor de saldo em um caixa ou conta bancária.

Finalidade: Este documento oferece uma visão arquitetural geral do sistema, usando diversas visões para representar diferentes aspectos do sistema como um todo. O objetivo deste documento é capturar e comunicar as decisões arquiteturais significativas que foram tomadas no decorrer do processo de desenvolvimento.

Escopo: Este documento auxilia os envolvidos no projeto a captar aspectos arquiteturais do sistema que são necessários para o desenvolvimento de uma solução que atenda às necessidades dos usuários finais. Além de auxiliar no entendimento do sistema por membros da equipe, seja da área técnica ou de negócio.

Definições, Acrônimos e Abreviações: • MVC = As siglas significam Model, View and Controller, é um padrão de projeto de desenvolvimento de software mundialmente conhecido para organizar de forma lógica todo o código do projeto. Onde M é onde colocamos todas as classes de domínio e regras de negócio. V é onde colocamos toda a parte de visualização da aplicação, ou seja, a parte visível a interface onde o usuário final irá interagir e por fim o C que colocamos todos os controladores que farão acesso a camada Model para executar alguma ação como por exemplo salvar um registro no banco de dados. • POO = Programação Orientada a Objetos, um paradigma das linguagens de programação que tentam imitar o mundo real, com conceitos base de classes, atributos e métodos. Herança, polimorfismo e sobrecarga de métodos são conceitos mais avançados que também compreende a Orientação a Objetos • DI = Injeção de Dependência, padrão de projeto onde à uma inversão de controle na instanciação de Objetos, ficando por responsabilidade do framework que esta sendo usado • Pattern Designers = Termo muito utilizado no âmbito de desenvolvimento de software para referir a um padrão de design mundialmente testado e validado para resolver um certo tipo de problema no mundo da programação.

Representação Arquitetural: • Visão de caso de uso o Apresenta as funcionalidades importantes e os usuários do sistema envolvido • Visão lógica o Descreve as classes e sua organização e apresenta o padrão de arquitetura que deverá ser utilizado para o desenvolvimento do sistema • Visão de processos o Mostra o padrão de comportamento do sistema diante de diferentes ações do usuário • Visão de implantação o Descreve a estrutura do ambiente onde o software será instalado • Visão de implementação o Ilustra a distribuição do processamento em um conjunto de nós do sistema, incluindo a distribuição física dos processos e threads.

Metas e Restrições da Arquitetura: Existem algumas restrições de requisitos do sistema com a seguinte arquitetura: • Utilização do paradigma POO • Estrutura MVC • PatternDesigners = (Facade, Builder, DI, etc...) • Banco de dados em memória H2 • Linguagem de programação Java • API RESTFul • Spring Boot • Linguagem de programação Java 11 • Multiplataforma

Visão de Caso de Uso Os casos de uso do sistema de Controle de Caixa serão listados abaixo:

controle-caixa/docs/diagrama.jpeg

Visão Lógica: A visão lógica define a estrutura da arquitetura. Abaixo será especificado o padrão utilizado para o desenvolvimento do sistema, no caso, MVC.

controle-caixa/docs/visaoLogica.jpeg

Visão de Implantação: O sistema será implantado em um sistema operacional de preferência do usuário, lembrando que uma restrição da aplicação e rodar em multiplataforma.

Visão de Implementação: O sistema será implementado utilizando conceitos de Orientação a Objetos, utilizando como principal linguagem de programação o Java na sua versão 11, utilizaremos também o framework Spring Boot para criação das APIs RESTFul. A aplicação rodará em um Container WEB (Tomcat) embutido e empacotado a um arquivo .jar. Banco de dados em memória H2.

Este projeto foi criado baseado nas melhores praticas de mercado, SOLID, CLEAN CODE, usei arquitetura de serviços com tratamento de erros, para este projeto utilizamos o banco de dados H2

Gostaria de ter incluido DOCKER E KUBERNETES PARA CLUSTERIZACAO

GOSTARIA de ter incluido tratativa com FILAS Kafka ou rabbit mp

gostaria de poder usar server config ja inclusa na arquitetura proposta com springboot, porem como nao possuo cloud nao foi poossivel, poderiamos usar tambem, load balance do EUREKA e zuuul gateway PARA ACESSO AOS SERVIÇOS DO PROJETO.

Tratar testes unitario e de integraçao... dentre outros

Para executar o projeto rode a classe Main com um perfil selecionado:

/controlecaixa/src/main/java/br/com/w2s/api/controlecaixa/ControleCaixaApplication.java

O sistema roda por padrão na porta 8080 com o contextPath /api/v1

O path do banco h2 é /h2-console

Na pasta /docs esta as collections para teste via Postman

Para acessar a documentação Swagger acesse /api/v1/swagger-ui.html

Qualquer dúvidas entrar em contato atravéz do email

rodrigo.mazoni@gmail.com